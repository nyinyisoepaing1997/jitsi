### Quick start
In order to quickly run Jitsi Meet on a machine running Docker and Docker Compose, follow these steps:



Create a .env file by copying and adjusting env.example:
```bash
cp env.example .env

# Public URL for the web service (required)
PUBLIC_URL=https://meet.domain.com

```
Set strong passwords in the security section options of .env file by running the following bash script
```
./gen-passwords.sh
```
Create required CONFIG directories
```
mkdir -p ./.jitsi-meet-cfg/{web,transcripts,prosody/config,prosody/prosody-plugins-custom,jicofo,jvb,jigasi,jibri}
```

```
docker-compose up -d -f docker-compose-all.yml
```
Access the web UI at http://localhost:8000 (or a different port, in case you edited the .env file).


### For Nginx Proxy
```
upstream jitsi_web {
    server localhost:8000;
}

server {
    listen 80;
	charset utf-8;
	server_name meet.baganchat.com;

        location / {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $host;
		        proxy_pass http://jitsi_web;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";

                #try_files $uri $/index.html;
        }

        location /xmpp-websocket {
            proxy_pass http://jitsi_web;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
        }
        location /colibri-ws {
            proxy_pass http://jitsi_web;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
        }
}  
```

### Using jitsi on multiple servers
```bash
# jitsi/web , jitsi/prosody, jitsi/jicofo
docker-compose -f docker-compose-jitsi.yml up -d

# jitsi/jvb
# update extra_hosts ip as prosody server ip 
docker-compose -f docker-compose-jvb.yml up -d 
```